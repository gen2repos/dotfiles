function on_win_open(win)

	function error(s)
		vis:info(s)
	end

	function build()
		os.execute(string.format('setsid tectonic %s >/dev/null 2>&1 &', win.file.name))
		vis:info(string.format('compiling %s with tectonic', win.file.name))
	end

	function open()
		local file = win.file.name
		local base = string.match(file, '.*%.')
		os.execute(string.format('setsid zathura %spdf >/dev/null 2>&1 &', base))
		vis:info(string.format('opening %spdf in zathura', base))
	end
	
	vis:map(vis.modes.NORMAL, '<C-l>', build)
	vis:command_register('build', build)
	vis:map(vis.modes.NORMAL, '<C-o>', open)
end

vis.events.subscribe(vis.events.WIN_OPEN, on_win_open)
