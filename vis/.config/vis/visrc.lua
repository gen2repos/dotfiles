-- load standard vis module, providing parts of the Lua API
require('vis')

-- load plugins
require('plugins/spellcheck')
require('plugins/autobuild')

-- set tty variable for determining usuble colors
local handle = io.popen("tty")
local output = handle:read("*a")
handle:close()
if ( output:match "tty" )
then
tty = true
end

vis.events.subscribe(vis.events.INIT, function()
	-- Your global configuration options
	vis:command('set theme iceberg')
	vis:map(vis.modes.INSERT, '<C-v>', '<Escape>"+pi')
	-- vis:command('set syntax on')
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	-- Your per window configuration options e.g.
	vis:command('set number')
	vis:command('set autoindent on')
	vis:command('set cursorline on')
	vis:command('set tabwidth 8')
end)
